package example.sim;

import java.util.*;
import hc.*;


class Soda extends Beam {
  // total number of days
  final public static int NUM = Model.intConst("N");
  // maximum price
  final public static UNITS = 20;

  // initial price
  final public static INIT_P = 10;

  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public Soda() {
    super();
  }
  public void run() {
    for(; flips < N; ++flips) {
      if(unknown(2, flips, heads, N) == 0) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
    }
  }
  public static void main(String[] args) {
    Soda cf = new Soda();
    Model.name(cf);
    cf.start();
    Model.check("", "Pmax=? [F flips=N & heads=N/2]");
  }
}

class Client extends Beam {
}
