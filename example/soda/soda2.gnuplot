set terminal epslatex monochrome dashed size 1,1 font ',13'
set size 3, 2.6
set output 'to1plot.tex'
set xlabel '\(p_2\)' offset 0,0.8
set ylabel '\(to1(p_1, p_2)\)' offset 1.7
set xrange [0:20]
set yrange [0:1]
set key below
set samples 2000

max(x,y) = (x > y) ? x : y
min(x,y) = (x < y) ? x : y
pow(x,y) = x**y

plot \
	0.5 + 0.5*pow(max(-(x - 5)/20.0, (x - 5)/20.0), 0.2)*(x > 5 ? 1.0 : -1.0) \
	with lines title '\(p_1 = \,\,\,5\)', \
	0.5 + 0.5*pow(max(-(x - 10)/20.0, (x - 10)/20.0), 0.2)*(x > 10 ? 1.0 : -1.0) \
	with lines title '\(p_1 = 10\)', \
	0.5 + 0.5*pow(max(-(x - 19)/20.0, (x - 19)/20.0), 0.2)*(x > 19 ? 1.0 : -1.0) \
	with lines title '\(p_1 = 19\)'
