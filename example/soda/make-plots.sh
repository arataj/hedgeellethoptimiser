gle -tex -fullpage -output a.pdf soda2.gle ../../../Modelling/soda2 24
pdfcrop --margins "0 -246 0 0" a.pdf soda2-key.pdf
pdfcrop --margins "0 0 0 -16" a.pdf soda2-24.pdf
gle -tex -output a.pdf soda2.gle ../../../Modelling/soda2 36
pdfcrop --margins "0 0 0 -16" a.pdf soda2-36.pdf
rm a.pdf
Rscript paths.R Soda
pdfcrop --margins "0 0 0 0" Soda.pdf soda2-paths.pdf
rm Soda.pdf
Rscript paths-4.R soda2
pdfcrop --margins "0 0 0 0" soda2-tmp.pdf soda2-paths12.pdf
rm soda2-tmp.pdf
Rscript paths-36.R soda2-36
pdfcrop --margins "0 0 0 0" soda2-36-tmp.pdf soda2-paths36.pdf
rm soda2-36-tmp.pdf
