package example.sim;

import java.util.*;
import hc.*;

class CoinFlip2 extends Beam {
  final static int N = Model.intConst("N");
  int /*@(0, N+1)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  int /*@(0,1)*/ last=0;
  
  public CoinFlip2() {
    super();
  }
  public void run() {
    for(; flips < N; ++flips) {
      if(unknown(2, flips, heads, last, N) == 0) {
        if(Math.random() < 0.5)
          ++heads;
        last=0;
      } else {
        if(Math.random() < 0.6)
          ++heads;
        last=1;
      }
    }
    flips = N + 1;
  }
  public static void main(String[] args) {
    CoinFlip2 cf = new CoinFlip2();
    Model.name(cf);
    cf.start();
    //Model.check("", "Pmax=? [F flips=N & heads=N/2]");
    Model.check("", "Rmax=? [F flips=N+1]");
  }
}

/*@modelAppend(

rewards "r"
   flips=N : (10.0/(N/2))*(N/2-max(heads-N/2,N/2-heads));
   s0=2    : (1.0/N)*(1-last);
   s0=4    : (1.0/N)*last;
endrewards

)*/
