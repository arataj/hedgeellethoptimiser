package example.sim;

import java.util.*;
import hc.*;

class CoinFlip extends Beam {
  final static int N = Model.intConst("N");
  final static int M = 20;
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public CoinFlip() {
    super();
  }
  protected boolean decide() {
    return unknown(2, N, flips, M, heads) == 0;
  }
  public void run() {
    init();
    for(; flips < N; ++flips) {
      if(decide()) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
      step();
    }
    done();
  }
  public static void main(String[] args) {
    CoinFlip cf = new CoinFlip();
    Model.name(cf);
    cf.start();
    Model.check("", "Pmax=? [F flips=N & heads=N/2]");
  }
}
