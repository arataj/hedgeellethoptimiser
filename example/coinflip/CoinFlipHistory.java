package example.sim;

import java.util.*;
import hc.*;

class CoinFlipHistory extends Beam {
  final static int N = Model.intConst("N");
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  int /*@(0, 1)*/latestHead = 0;
  
  public CoinFlipHistory() {
    super();
  }
  public void run() {
    for(; flips < N; ++flips) {
      boolean head = false;
      if(unknown(2, flips, heads, latestHead, N) == 0) {
        if(latestHead == 0) {
          if(Math.random() < 0.55)
            head = true;
        } else {
          if(Math.random() < 0.45)
            head = true;
        }
      } else {
        if(latestHead == 0) {
          if(Math.random() < 0.65)
            head = true;
        } else {
          if(Math.random() < 0.55)
            head = true;
        }
      }
      if(head) {
        ++heads;
        latestHead = 1;
      } else
        latestHead = 0;
    }
  }
  public static void main(String[] args) {
    CoinFlipHistory cf = new CoinFlipHistory();
    Model.name(cf);
    cf.start();
    Model.check("", "Pmax=? [F flips=N & heads=N/2]");
  }
}

