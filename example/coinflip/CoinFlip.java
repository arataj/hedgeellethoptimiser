package example.sim;

import java.util.*;
import hc.*;

class CoinFlip extends Beam {
  final static int N = Model.intConst("N");
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public CoinFlip() {
    super();
  }
  public void run() {
    for(; flips < N; ++flips) {
      if(unknown(2, flips, heads, N) == 0) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
    }
  }
  public static void main(String[] args) {
    CoinFlip cf = new CoinFlip();
    Model.name(cf);
    cf.start();
    Model.check("", "Pmax=? [F flips=N & heads=N/2]");
  }
}
