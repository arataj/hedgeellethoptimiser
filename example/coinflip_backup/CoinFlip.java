package example.sim;

import java.util.*;
import hc.*;

import static example.sim.CoinChoice.*;

class CoinFlip extends Beam {
  final static int N = 100;
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public CoinFlip() {
    super();
  }
  protected boolean decide() {
    if(Model.isStatistical())
      return A*flips + B > C;
    else
      return random.nextInt(2) == 0;
  }
  public void run() {
    init();
    for(; flips < N; ++flips) {
      if(decide()) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
      step();
    }
    done();
  }
  public static void main(String[] args) {
    CoinFlip cf = new CoinFlip();
    Model.name(cf);
    cf.start();
    Model.check("", "Pmax=? [F flips=N & heads=N/2]");
  }
}
