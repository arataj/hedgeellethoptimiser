package example.sim;

import java.lang.reflect.Field;

public class CoinFlipState {
    private static Field getField(Class clazz, String fieldName)
            throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
    hc.AbstractBeam o1;
    Field f2;
    hc.AbstractBeam o4;
    Field f5;
    public CoinFlipState() {
        try {
            Field f0 = getField(hc.AbstractBeam.class, "staticHook");
            f0.setAccessible(true);
            hc.AbstractBeam[] o0 = (hc.AbstractBeam[])f0.get(null);
            o1 = o0[0];
            f2 = getField(example.sim.CoinFlip.class, "flips");
            f2.setAccessible(true);
            Field f3 = getField(hc.AbstractBeam.class, "staticHook");
            f3.setAccessible(true);
            hc.AbstractBeam[] o3 = (hc.AbstractBeam[])f3.get(null);
            o4 = o3[0];
            f5 = getField(example.sim.CoinFlip.class, "heads");
            f5.setAccessible(true);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int flips() {
        try {
            return f2.getInt(o1);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int heads() {
        try {
            return f5.getInt(o4);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    public double get(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int getInt(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public long getLong(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int size() {
        return 2;
    }
    public String getName(int index) {
        switch(index) {
            case 0:
                return "flips";
            case 1:
                return "heads";
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
}
