/*
 * HedgeellethOptimiser.java
 *
 * Created on Jun 6, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.iitis.hedgeelleth.optimiser.approx.FormalExtrapolate;

/**
 *
 * @author Artur Rataj
 */
public class Main {
    /**
     * A prefix added befor each CLI message.
     */
    protected static final String MESSAGE_PREFIX = "verics optimiser";
    /**
     * CLI.
     * 
     * @param args CLI arguments
     */
    public static void main(String[] args) {
        String[] args_ = {
            "-F", "N=80#100#120", "-A", "N=200~500:100", "-mfe", "-fp", "-an",
            "-C-v0",  "-C-sh",  "-C-i",
            "example/coinflip/CoinFlip2.java", "-d1",
            //"/home/art/projects/svn/HedgeellethCompiler/regression/ndfile/CoinFlipChoice3.java",
            "-K"
        };
        String[] args_p = { "-w",
            "-F", "MAX=50#100#200", "-A", "NUM=200~500:100", "-mfe", "-fp", "-an",
            "example/soda/park.nm",
            "-d1", // "-Bc",
            "-K"
        };
        String[] args__ = {
            "--prism-executable", "prismg",
            "-F", "NUM=14", "-A", "NUM=200~500:100", "-mfe", "-fp", "-ab5",
            "example/soda/soda2.nm",
            "-d2", "-Bc",
            "-K"
        };
        args = args_;
        Options options = null;
        List<String> temporaryFiles = new LinkedList<>();
        try {
            options = new Options(args);
            if(!options.help) {
                if(options.files.isEmpty())
                    throw new CompilerException(null, CompilerException.Code.MISSING,
                            "no input files");
                OptimisedModel model = FormalModelMerge.merge(options, temporaryFiles);
                //
                // runs the checker, then the internal optimiser
                //
                AbstractOptimiser optimise;
                switch(options.method) {
                    case FORMAL_EXTRAPOLATE:
                        optimise = new FormalExtrapolate(options, model);
                        break;
                        
                    default:
                        throw new RuntimeException("unknown optimisation method");
                }
                optimise.process();
            }
        } catch(CompilerException e) {
            System.out.println(e.getMessage(MESSAGE_PREFIX));
            System.exit(1);
        } finally {
            if(options != null && !options.keepTemporaryFiles)
                for(String fileName : temporaryFiles) {
                    File f = new File(fileName);
                    f.delete();
                }
        }
    }

}
