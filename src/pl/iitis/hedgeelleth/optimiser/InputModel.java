/*
 * InputModel.java
 *
 * Created on 9 janv. 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.HcOutputFiles;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO;

/**
 * Describes the optimiser's input model. It is not necessarily the model specified
 * by user, as that one may require further translation into the input file.
 * 
 * @author Artur Rataj
 */
public class InputModel {
    /**
     * All temporary files; these can eventually be deleted. Empty lit
     * for no such files.
     */
    final List<String> temporaryFiles;
    /**
     * Input model file, typically a Prism model file.
     */
    final String modelFile;
    /**
     * Input property file, typically a Prism property file.
     */
    final String propertyFile;
    /**
     * An Nd file describing the input model.
     */
    final String ndFile;
    /**
     * Type of the input model.
     */
    final ModelType modelType;
    /**
     * Defines missing in the model, in the format &lt;name&gt;=&lt;value&gt;.
     * Empty for Hc-generated files as Hc completes the missing defines automatically.
     */
    private List<String> externalDefines;
    
    /**
     * Describes an input model made out of Hc output files.
     * 
     * @param hcOut represents fiels produced by a Hc run
     */
    public InputModel(HcOutputFiles hcOut) {
        temporaryFiles = hcOut.getGeneratedFiles();
        if(hcOut.modelFiles.size() != 1)
            throw new RuntimeException("expected a single model file");
        modelFile = hcOut.modelFiles.get(0);
        if(hcOut.propertyFiles.size() != 1)
            throw new RuntimeException("expected a single property file");
        propertyFile = hcOut.propertyFiles.get(0);
        modelType = hcOut.modelType;
        ndFile = hcOut.ndFile;
        externalDefines = new LinkedList<>();
    }
    /**
     * Describes an input model given directly by the user.
     * 
     * @param modelFile file name of the input model
     * @param propertyFile file name of the properties of the input model
     * @param ndFile nd file
     * @param externalDefines defines missing in the model, in the format
     * &lt;name&gt;=&lt;value&gt;
     */
    public InputModel(String modelFile, String propertyFile, String ndFile,
            List<String> externalDefines) throws CompilerException {
        temporaryFiles = new LinkedList<>();
        this.modelFile = modelFile;
        this.propertyFile = propertyFile;
        this.ndFile = ndFile;
        try {
            modelType = PTAIO.readType(modelFile);
        } catch(CompilerException e) {
            e.completeStreamName(modelFile);
            throw e;
        }
        this.externalDefines = externalDefines;
    }
    /**
     * Returns the temporary files, i.e. which can be deleted after use.
     * 
     * @return list of files, empty if no files
     */
    public List<String> getTemporaryFiles() {
        return temporaryFiles;
    }
    /**
     * Returns the input model file.
     * 
     * @return a file name
     */
    public String getModelFile() {
        return modelFile;
    }
    /**
     * Returns the input property file.
     * 
     * @return a file name
     */
    public String getPropertyFile() {
        return propertyFile;
    }
    /**
     * Returns the model type.
     * 
     * @return type of the input model
     */
    public ModelType getType() {
        return modelType;
    }
    /**
     * Returns the Nd file (a description of non-determinism).
     * 
     * @return a file name
     */
    public String getNdFile() {
        return ndFile;
    }
    /**
     * Returns the external defines. See the field <code>externalDefines</code>
     * for details.
     * 
     * @return a list of named defines in the format &lt;name&gt;=&lt;value&gt; each;
     * empty list for no external defines
     */
    public List<String> getExternalDefines() {
        return externalDefines;
    }
}
