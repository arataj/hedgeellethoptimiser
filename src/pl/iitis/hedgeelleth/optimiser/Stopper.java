/*
 * Stopper.java
 *
 * Created on Jan 13, 2017
 *
 * Copyright (c) 2017  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

/**
 * A simple time-measuring and reporting class.
 * @author Artur Rataj
 */
public class Stopper {
    protected long startTime;
    
    public Stopper() {
        startTime = System.currentTimeMillis();
    }
    public String stop() {
        long estimatedTime = System.currentTimeMillis() - startTime;
        return String.format("%.1f", estimatedTime/1000.0) + " s";
    }
}
