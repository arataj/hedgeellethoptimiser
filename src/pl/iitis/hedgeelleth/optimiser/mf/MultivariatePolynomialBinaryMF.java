/*
 * LinearMF.java
 *
 * Created on Jan 10, 2017
 *
 * Copyright (c) 2017 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser.mf;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile;
import pl.gliwice.iitis.hedgeelleth.math.MultivariatePolynomial;
import pl.iitis.hedgeelleth.optimiser.OptimisedModel;
import pl.iitis.hedgeelleth.optimiser.OptimiserException;

/**
 * <p>A polynomial multivariate function per each choice, then binarised into 0 and 1.
 * Takes into account only state variables of the nd functions, parameters are ignored.</p>
 * 
 * <p>For <code>n - 1 state variables</code> the equation optimised has the form
 * </code>0.5 + Math.tanh(f(svs))/2.0</code>, where f(svs) is a polyvariate polynomial
 * of state variables, and the polynomial's coefficients are optimised.</p>
 * 
 * @author Artur Rataj
 */
public class MultivariatePolynomialBinaryMF extends AbstractRandomiseMF {
    /**
     * Order of the polynomials.
     */
    int ORDER;
    /**
     * Number of choices.
     */
    int NUM_CHOICES;
    /**
     * Number of coefficients in subsequent choices.
     */
    int[] cNumCoeff;
    /**
     * For each choice a respective boundary represented by a polynomial.
     */
    Map<NdFile.Choice, MultivariatePolynomial> boundaries;
    
    /**
     * Creates a new polynomial mf to be optimised. Initializes the optimised coefficients
     * with small values in the range &lt;0.0, 0.1).
     * 
     * @param model data of the model to optimise
     * @param order order, 1 for a linear combination
     */
    public MultivariatePolynomialBinaryMF(OptimisedModel model, int order) throws OptimiserException {
        // very small values as clamping with tanh may prematurely stop the optimiser
        super(model, 0.0, 0.02);
        String error = model.check01Choices();
        if(error != null)
            throw new OptimiserException(error);
        ORDER = order;
        if(ORDER < 1)
            throw new OptimiserException("order must be at least 1");      
        NUM_CHOICES = MODEL.ND_FILE.CHOICES.size();
        createCoeff();
        initAllCoeff();
    }
    private void createCoeff() {
        cNumCoeff = new int[NUM_CHOICES];
        boundaries = new HashMap<>();
        int choiceCount = 0;
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
            int numSV = ch.getNumSV();
            MultivariatePolynomial poly = new MultivariatePolynomial(ORDER, numSV);
            boundaries.put(ch, poly);
//            int numCoeff = poly.NUM_COEFF;
//            double[] v = poly.COEFF;
            // make the c
            coeff.put(ch, poly.COEFF);
            cNumCoeff[choiceCount] = poly.NUM_COEFF;
            ++choiceCount;
        }
    }
    @Override
    public int getNumCoeff() {
        int size = 0;
        int count = 0;
        for(int i = 0; i < NUM_CHOICES; ++i)
            size += cNumCoeff[i];
        return size;
    }
    @Override
    public double getValue(int[] state, NdFile.Choice ch, int[] varIndex)
            throws OptimiserException {
        MultivariatePolynomial poly = boundaries.get(ch);
        //double value = 0.5 + sum;
        double value = 0.5 + Math.tanh(poly.getValue(state, varIndex)*1)/2.0;
        //double value = 0.5 + 0.9999*Math.tanh(sum*1)/2.0 + 0.0001*sum;
        //double value = sum > 0 ? 1 : 0;
        return value;
    }
    @Override
    public double getErrorComponent(double actual, int[] expected)
            throws OptimiserException {
        if(expected.length != 1)
            throw new OptimiserException("LinearMF supports only single--valued " +
                    "nd branches");
        double diff = Math.abs(actual - expected[0]);
        return diff*diff;
    }
    @Override
    public String getClassDescr() {
        return "binary multivariate polynomial order " + ORDER;
    }
    @Override
    public String toString(MultivariatePolynomial.OutFormat outFormat) {
        StringBuilder out = new StringBuilder();
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
            out.append("choice " + ch.range.toString());
            out.append("\n");
            MultivariatePolynomial poly = boundaries.get(ch);
            out.append(poly.toString(ch.argSource, null, outFormat));
            if(outFormat != MultivariatePolynomial.OutFormat.FIND_MULTIVARIATE_POLY_POWERS)
                out.append(" > 0 ? 1 : 0\n");
        }
        return out.toString();
    }
}
