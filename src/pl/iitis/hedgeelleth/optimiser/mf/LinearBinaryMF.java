/*
 * LinearMF.java
 *
 * Created on Junn 20, 2016
 *
 * Copyright (c) 2016 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser.mf;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile;
import pl.gliwice.iitis.hedgeelleth.math.MultivariatePolynomial;
import pl.iitis.hedgeelleth.optimiser.OptimisedModel;
import pl.iitis.hedgeelleth.optimiser.OptimiserException;

/**
 * <p>A linear multivariate function per each choice, then binarised into 0 and 1.
 * Takes into account only state variables of the nd functions, parameters are ignored.</p>
 * 
 * <p>For <code>n - 1 state variables</code> the equation optimised has the form
 * </code>0.5 + Math.tanh(f(svs))/2.0</code>, where f(svs) is a linear combination
 * of state variables a0*sv0 + a1*sv1 + ... + an and ai are  optimised.</p>
 * 
 * @author Artur Rataj
 */
public class LinearBinaryMF extends AbstractRandomiseMF {
    /**
     * Creates a new affine mf to be optimised. Initializes the optimised coefficients
     * with small values in the range &lt;0.0, 0.1).
     * 
     * @param model data of the model to optimise
     */
    public LinearBinaryMF(OptimisedModel model) throws OptimiserException {
        // very small values as clamping with tanh may prematurely stop the optimiser
        super(model, 0.0, 0.01);
        String error = model.check01Choices();
        if(error != null)
            throw new OptimiserException(error);
        createCoeff();
        initAllCoeff();
    }
    private void createCoeff() {
        // a single coefficient for each variable plus a single free coefficient
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
            double[] v = new double[ch.getNumSV() + 1];
            coeff.put(ch, v);
        }
    }
    @Override
    public int getNumCoeff() {
        int size = 0;
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES)
            size += ch.getNumSV() + 1;
        return size;
    }
    @Override
    public double getValue(int[] state, NdFile.Choice ch, int[] varIndex)
            throws OptimiserException {
        double sum = 0.0;
        int svCount = 0;
        double[] c = coeff.get(ch);
        for(int argIndex = 0; argIndex < varIndex.length; ++argIndex) {
            int svIndex = varIndex[argIndex];
            if(svIndex != -1) {
                // it is not a parameter, but a state variable
                sum += c[svCount++]*state[svIndex];
            }
        }
        // the last, free coefficient
        sum += c[svCount];
        double value = 0.5 + Math.tanh(sum)/2.0;
        //double value = sum > 0 ? 1 : 0;
        return value;
    }
    @Override
    public double getErrorComponent(double actual, int[] expected)
            throws OptimiserException {
        if(expected.length != 1)
            throw new OptimiserException("LinearMF supports only single--valued " +
                    "nd branches");
        double diff = Math.abs(actual - expected[0]);
        return diff*diff;
    }
    @Override
    public String getClassDescr() {
        return "binary hyperplane";
    }
    @Override
    public String toString(MultivariatePolynomial.OutFormat outFormat) {
        StringBuilder out = new StringBuilder();
        switch(outFormat) {
            case PLAIN:
            case JAVA:
                for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
                    out.append("choice " + ch.range.toString());
                    out.append("\n\t");
                    double[] c = coeff.get(ch);
                    int[] varIndex = MODEL.VAR_INDEX.get(ch);
                    int coeffCount = 0;
                    boolean first = true;
                    for(int argCount = 0; argCount < varIndex.length; ++argCount) {
                        int svIndex = varIndex[argCount];
                        if(svIndex != -1) {
                            double mult = c[coeffCount++];
                            if(mult >= 0) {
                                if(!first)
                                    out.append(" + ");
                            } else {
                                mult = Math.abs(mult);
                                if(first)
                                    out.append("-");
                                else
                                    out.append(" - ");
                            }
                            out.append(mult + "*" + ch.argSource.get(argCount));
                            first = false;
                        }
                    }
                    double free = c[coeffCount];
                    if(free >= 0) {
                        out.append(" + ");
                    } else {
                        free = Math.abs(free);
                        out.append(" - ");
                    }
                    out.append(free);
                }
                out.append(" > 0 ? 1 : 0\n");
                break;
                
            case FIND_MULTIVARIATE_POLY_POWERS:
                out.append(printCoefficientsArrayStyle());
                break;
                
            default:
                throw new RuntimeException("unknown output format");
        }
        return out.toString();
    }
}
