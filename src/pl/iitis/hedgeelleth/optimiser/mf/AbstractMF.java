/*
 * AbstractMF.java
 *
 * Created on Jun 20, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser.mf;

import java.util.*;
import org.apache.commons.math3.analysis.MultivariateFunction;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile;
import pl.gliwice.iitis.hedgeelleth.math.MultivariatePolynomial;
import pl.iitis.hedgeelleth.optimiser.OptimisedModel;
import pl.iitis.hedgeelleth.optimiser.OptimiserException;

/**
 * Abstract multivariate equations behind nd functions.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractMF implements MultivariateFunction {
    /**
     * Data of the model to optimise.
     */
    protected final OptimisedModel MODEL;
    /**
     * Coefficients of parameters and arguments of nd functions. The map is
     * created in this class' constructor.
     */
    public final Map<NdFile.Choice, double[]> coeff;
    /**
     * Creates a new mf to be optimised.
     * 
     * @param model data of the model to optimise
     */
    public AbstractMF(OptimisedModel model) {
        MODEL = model;
        coeff = new HashMap<>();
    }
    /**
     * Must be called in a constructor of a class implementing
     * <code>initCoeff()</code>. The coefficients must be already
     * created. Calls <code>initCoeff()</code>.
     */
    protected final void initAllCoeff() {
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
            double[] v = coeff.get(ch);
            initCoeff(ch, v);
        }
    }
    /**
     * Sets initial values of coefficients for an optimised equation
     * behind a given choice. A class which implements this method
     * must include in its constructor this method's caller <code>initAllCoeff()</code>.
     * 
     * @param ch choice
     * @param values this method writes initial values into this array
     */
    protected abstract void initCoeff(NdFile.Choice ch, double[] values);
    /**
     * Returns the total number of coefficients for all choices together.
     * Not called before <code>initCoeff()</code>.
     * 
     * @return number of coefficients, should match total length of arrays
     * allocated in <code>allocCoeff()</code>
     * 
     */
    public abstract int getNumCoeff();
    /**
     * Returns the current coefficients in a single array, ordered by
     * <code>MODEL.ND_FILE.CHOICES</code>.
     * 
     * @return a new instance of a point in the optimisation space
     */
    public double[] getPoint() {
        int size = getNumCoeff();
        double[] point = new double[size];
        int count = 0;
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
            double[] sub = coeff.get(ch);
            for(int i = 0; i < sub.length; ++i)
                point[count++] = sub[i];
        }
        if(count != size)
            throw new RuntimeException("unexpected");
        return point;
    }
    /**
     * Propagates vaules in a single array into coefficients
     * <code>MODEL.ND_FILE.CHOICES</code>.
     * 
     * @return a new instance of a point in the optimisation space
     */
    public void setPoint(double[] point) {
        int count = 0;
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
            double[] sub = coeff.get(ch);
            for(int i = 0; i < sub.length; ++i)
                sub[i] = point[count++];
        }
        if(count != point.length)
            throw new RuntimeException("unexpected");
    }
    /**
     * Returns a value of one of the optimised equations between nd functions.
     * 
     * @param state state, for which the value is computed
     * @param ch choice at that state; determines the equation to optimise
     * @param varIndex indexed by argument index of a respective nd function,
     * contains state variable index for a variable, a placeholder of -1 for a
     * parameter
     * @return value of the optimised equation
     */
    public abstract double getValue(int[] state, NdFile.Choice ch, int[] varIndex)
            throws OptimiserException;
    /**
     * Returns an error component for a determinism resolution at a given
     * state.
     * 
     * @param actual what an optimised equation returned. i.e. what
     * <p>getValue()</p> returns
     * @param expected non--determinism resolution values, which are optimal
     * @return value of the component; these values are summed over
     * whole resolution of non--determinisn, into what <code>getError()</code>
     * returns
     */
    protected abstract double getErrorComponent(double actual, int[] expected)
            throws OptimiserException;
    /**
     * Returns the averaged error over all choices, in respect to the expected
     * values.
     * 
     * @return averaged error
     */
    public double getError() throws OptimiserException {
        double sum = 0.0;
        Iterator<Integer> stateI = MODEL.CHOICE.STATE.iterator();
        Iterator<NdFile.Branch> branchI = MODEL.CHOICE.BRANCH.iterator();
        int count = 0;
        while(stateI.hasNext()) {
            int stateIndex = stateI.next();
            NdFile.Branch branch = branchI.next();
            int[] state = MODEL.STATES.getState(stateIndex);
            NdFile.Choice ch = branch.owner;
            int[] varIndex = MODEL.VAR_INDEX.get(ch);
            double actual = getValue(state, ch, varIndex);
            int[] expected = branch.numAsArray;
            double component = getErrorComponent(actual, expected);
            double[] c = coeff.get(ch);
            for(int i = 0; i < c.length; ++i) {
                
                //component += 1e-8*Math.abs(c[i]*10000 - Math.round(c[i]*10000));
            }
            sum += component;
            ++count;
        }
        return sum/count;
    }
    /**
     * Returns the ratio of hits over all choices.
     * 
     * @return 0 ... 1
     */
    public double getHitsRatio() throws OptimiserException {
        int hits = 0;
        Iterator<Integer> stateI = MODEL.CHOICE.STATE.iterator();
        Iterator<NdFile.Branch> branchI = MODEL.CHOICE.BRANCH.iterator();
        int count = 0;
        while(stateI.hasNext()) {
            int stateIndex = stateI.next();
            NdFile.Branch branch = branchI.next();
            int[] state = MODEL.STATES.getState(stateIndex);
            NdFile.Choice ch = branch.owner;
            int[] varIndex = MODEL.VAR_INDEX.get(ch);
            double actual = getValue(state, ch, varIndex);
            if(branch.numAsArray.length != 1)
                throw new RuntimeException("expected a single expected value");
            int expected = branch.numAsArray[0];
            if(expected > 1)
                throw new RuntimeException("expected a binary choice");
            if((actual >= 0.5) == (expected == 1))
                ++hits;
            ++count;
        }
        return hits*1.0/count;
    }
    @Override
    public double value(double[] point) {
        int pointCount = 0;
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
            double[] c = coeff.get(ch);
            //int[] varIndex = MODEL.VAR_INDEX.get(ch);
            int coeffCount = 0;
            //for(int argCount = 0; argCount < varIndex.length; ++argCount) {
            for(int i = 0; i < c.length; ++i)
                c[coeffCount++] = point[pointCount++];
        }
        if(pointCount != point.length)
            throw new RuntimeException("point dimension mismatch, found " +
                    point.length + ", expected " + pointCount);
        try {
            double error = getError();
//for(double d : point)
//    System.out.print(" " + d);
//System.out.println(" " + error);
            return error;
        } catch(OptimiserException e) {
            throw new RuntimeException(
                    "optimiser exception: " + e.getMessage());
        }
    }
    /**
     * A description of the class of this optimiser.
     * 
     * @return a short description
     */
    public abstract String getClassDescr();
    /**
     * Print coefficients in the default order, int the style of Java constant
     * array declaration. A helper method for <code>toString()</code>.
     * 
     * @return a textual description
     */
    protected String printCoefficientsArrayStyle() {
        StringBuilder out = new StringBuilder();
        for(NdFile.Choice ch : MODEL.ND_FILE.CHOICES) {
            double[] c = coeff.get(ch);
            out.append("{\n");
            boolean first = true;
            for(double v : c) {
                if(first)
                    first = false;
                else
                    out.append(",\n");
                out.append('\t');
                out.append(v);
            }
            out.append("\n}\n");
        }
        return out.toString();
    }
    /**
     * Prints this function.
     * 
     * @param javaStyle if to use Java math functions
     * @return textual representation
     */
    public abstract String toString(MultivariatePolynomial.OutFormat outFormat);
    @Override
    public String toString() {
        return toString(MultivariatePolynomial.OutFormat.PLAIN);
    }
}
