/*
 * AbstractRandomiseMF.java
 *
 * Created on Jun 20, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser.mf;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile;
import pl.iitis.hedgeelleth.optimiser.OptimisedModel;

/**
 * Sets initial coefficients with random values.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractRandomiseMF extends AbstractMF {
    /**
     * Minimum initial value of a coefficient.
     */
    final double RANDOM_MIN;
    /**
     * Maximum initial value of a coefficient.
     */
    final double RANDOM_MAX;
    
    /**
     * Creates a new mf to be optimised. Does not call
     * <code>initAllCoeff()</code>.
     * 
     * @param model data of the model to optimise
     * @param randomMin minimum initial value of a coefficient, inclusive
     * @param randomMax maximum initial value of a coefficient, exclusive
     */
    public AbstractRandomiseMF(OptimisedModel model,
            double randomMin, double randomMax) {
        super(model);
        RANDOM_MIN = randomMin;
        RANDOM_MAX = randomMax;
    }
    /**
     * Returns a minimum initial value of a coefficient.
     * 
     * @return minimum initial value
     */
    public double getRandomMin() {
        return RANDOM_MIN;
    }
    /**
     * Returns a maximum initial value of a coefficient.
     * 
     * @return maximum initial value
     */
    public double getRandomMax() {
        return RANDOM_MAX;
    }
    @Override
    protected void initCoeff(NdFile.Choice ch, double[] values) {
        for(int i = 0; i < values.length; ++i)
            values[i] = RANDOM_MIN + (RANDOM_MAX - RANDOM_MIN)*Math.random();
    }
}
