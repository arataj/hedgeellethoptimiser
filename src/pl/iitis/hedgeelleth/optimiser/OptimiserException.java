/*
 * OptimiserException.java
 *
 * Created on Jun 20, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

/**
 * An exception of the optimiser.
 * 
 * @author Artur Rataj
 */
public class OptimiserException extends Exception {
    public OptimiserException(String message) {
        super(message);
    }
}
