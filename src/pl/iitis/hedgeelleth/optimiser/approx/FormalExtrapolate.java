/*
 * Optimise.java
 *
 * Created on Jun 6, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser.approx;

import java.util.*;

import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.OptimizationData;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.SimpleBounds;
import org.apache.commons.math3.optim.SimplePointChecker;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.MultivariateOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.BOBYQAOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.MultiDirectionalSimplex;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer;
import org.apache.commons.math3.random.Well19937c;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.math.BFGSOptimizer;

import pl.iitis.hedgeelleth.optimiser.*;
import pl.iitis.hedgeelleth.optimiser.mf.*;

/**
 * A two stage optimisation: formal step for finding scalable initial conditions for
 * the approximate step.
 * 
 * @author Artur Rataj
 */
public class FormalExtrapolate extends AbstractOptimiser {
    public FormalExtrapolate(Options options, OptimisedModel model) {
        super(options, model);
    }
    private AbstractMF getNewMF(OptimisedModel model) throws OptimiserException {
        switch(OPTIONS.approxDecisionBoundary) {
            case HYPERPLANE:
                return new LinearBinaryMF(model);
                
            case POLYNOMIAL:
                return new MultivariatePolynomialBinaryMF(model,
                        OPTIONS.approxDecisionBoundaryPolynomialOrder);
                
            default:
                throw new OptimiserException("unknown class of the decision boundary");
        }
    }
    protected void optimise(OptimisedModel model) throws OptimiserException {
        Stopper stopper = new Stopper();
        double maxHr = -1;
        AbstractMF bestMf = null;
        for(int restart = 0; restart < Math.max(1, OPTIONS.approxRestarts); ++restart) {
            AbstractMF mf = getNewMF(model);
            MultivariateOptimizer optimizer;
            String optimiserClass;
            List<OptimizationData> optData = new LinkedList<>();
            int numCoeff = mf.getPoint().length;
            double[] step = new double[numCoeff];
            double initialStep;
            if(mf instanceof AbstractRandomiseMF)
                // a rather safe value to avoid initial tanh saturation
                initialStep = ((AbstractRandomiseMF)mf).getRandomMax()*0.2;
            else
                initialStep = 0.02;
            for(int i = 0; i < numCoeff; ++i)
                step[i] = initialStep;
            double[] lower = new double[numCoeff];
            double[] upper = new double[numCoeff];
            for(int i = 0; i < numCoeff; ++i) {
                lower[i] = -100.0;
                upper[i] = 100.0;
            }
            // default
            int MAX_ITER = (int)Math.round(60000*Math.log((1 + numCoeff)/4.0));
            InitialGuess initialGuess = new InitialGuess(mf.getPoint());
            switch(OPTIONS.approx) {
                case SIMPLEX_NELDER_MEAD:
                    optimizer = new SimplexOptimizer(1e-20, 1e-40);
                    optData.add(new NelderMeadSimplex(step));
                    optimiserClass = "Nelder-Mead simplex";
                    break;

                case SIMPLEX_MULTIDIRECTIONAL:
                    optimizer = new SimplexOptimizer(1e-20, 1e-40);
                    optData.add(new MultiDirectionalSimplex(step));
                    optimiserClass = "multidirectional simplex";
                    break;

                case BOBYQA:
                    optimizer = new BOBYQAOptimizer(numCoeff*2);
                    optData.add(new SimpleBounds(lower, upper));
                    optimiserClass = "BOBYQA";
                    break;

                case CMAES:
                    optimizer = new CMAESOptimizer(MAX_ITER, 0, true, 0, 10,
                           new Well19937c(), false,
                           new SimplePointChecker<>(1e-20, 1e-40));
                    optData.add(new SimpleBounds(lower, upper));
                    double[] range = new double[numCoeff];
                    for(int i = 0; i < numCoeff; ++i)
                        range[i] = (upper[i] - lower[i])/3.0;
                    OptimizationData sigma = new CMAESOptimizer.Sigma(range);
                    OptimizationData popSize = new CMAESOptimizer.PopulationSize((int)(4 + Math.floor(
                            3*Math.log(initialGuess.getInitialGuess().length))));
                    optData.add(sigma);
                    optData.add(popSize);
                    optimiserClass = "CMAES";
                    break;

                case BFGS:
                    optimizer = new BFGSOptimizer();
                    optimiserClass = "BGFSO";
                    throw new RuntimeException("BFGS unimplemented");
                    //break;

                default:
                    throw new RuntimeException("unknown optimiser class");
            }     
            if(OPTIONS.approxRestarts != 0) {
    //            optimizer = new MultiStartMultivariateOptimizer((MultivariateOptimizer)optimizer,
    //                    OPTIONS.approxRestarts,
    //                    new UncorrelatedRandomVectorGenerator(mf.getNumCoeff(),
    //                            new UniformRandomGenerator(new Well19937c())));
                optimiserClass += " with " + OPTIONS.approxRestarts + " restarts";
            }
            if(restart == 0)
                System.out.println("optimising " + mf.getClassDescr() + ", " +
                        "number of coeff = " + numCoeff + ", " +
                        "using " + optimiserClass + ", initial step = " + initialStep +
                        ", max iterations = " + MAX_ITER + "...");
            optData.add(new MaxEval(MAX_ITER));
            optData.add(new ObjectiveFunction(mf));
            optData.add(GoalType.MINIMIZE);
            optData.add(initialGuess);
            if(OPTIONS.approxRestarts != 0)
                System.out.print("\t#" + restart + ":");
            try {
                final PointValuePair optimum = optimizer.optimize(
                        optData.toArray(new OptimizationData[optData.size()]));
                mf.setPoint(optimum.getPoint());
            } catch(TooManyEvaluationsException e) {
                System.out.println("\ttoo many iterations, aborted");
                continue;
            }
    //        System.out.println(Arrays.toString(optimum.getPoint()) + " : "
    //            + optimum.getSecond());
            try {
                if(OPTIONS.approxRestarts != 0) {
                    double hr = mf.getHitsRatio();
                    System.out.println("\thits = " + hr + " error = " + mf.getError());
                    if(maxHr < hr) {
                        maxHr = hr;
                        bestMf = mf;
                    }
                } else
                    bestMf = mf;
            } catch(OptimiserException e) {
                throw new RuntimeException("unexpected: " + e.toString());
            }
        }
        System.out.println("optimalization took " + stopper.stop());
        if(bestMf == null)
            System.out.println("no solution found");
        else {
            try {
                System.out.println("best hits to merged strategy = " + bestMf.getHitsRatio()+
                        " at error = " + bestMf.getError());
            } catch(OptimiserException e) {
                throw new RuntimeException("unexpected: " + e.toString());
            }
            System.out.println(bestMf.toString(OPTIONS.boundaryEquationStyle));
        }
    }
    /**
     * The formal extrapolate method.
     */
    @Override
    protected void process() throws CompilerException {
        try {
            optimise(MODEL);
        } catch(OptimiserException e) {
            throw new CompilerException(null, CompilerException.Code.INVALID,
                    "could not optimise: " + e.getMessage());
        }
   }
}
