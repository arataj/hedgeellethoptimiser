/*
 * AbstractFormal.java
 *
 * Created on Jun 9, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser.formal;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * Runs a model checker, gathers the results.
 *  
 * @author Artur Rataj
 */
public abstract class AbstractFormal {
    /**
     * Options, does not include ant of the input files.
     */
    protected final String[] OPTIONS;
    /**
     * The model input files.
     */
    protected final String[] MODEL_FILES;
    /**
     * The property input files.
     */
    protected final String PROPERTY_FILE;
    /**
     * External defines, as specified in <code>InputModel.getExternalDefines()</code>;
     * empty list for none.
     */
    protected final List<String> EXTERNAL_DEFINES;
    /**
     * If to fail on a major warning from the checker. A failure in case of an error
     * is always unconditional.
     */
    protected final boolean FAIL_ON_WARNING;
    
    /**
     * Creates a new abstract model checker interface.
     * 
     * @param options options, does not include ant of the input files
     * @param modelFiles the model input files
     * @param propertyFile the property input files
     * @param externalDefines external defines ni the form as specified in
     * <code>InputModel.getExternalDefines()</code>; null or empty list for none
     * @param failOnWarning if to fail on a major warning from the checker; independently,
     * there is always a failure in case of an error
     */
    public AbstractFormal(String[] options, String[] modelFiles, String propertyFile,
            List<String> externalDefines, boolean failOnWarning) {
        OPTIONS = options;
        MODEL_FILES = modelFiles;
        PROPERTY_FILE = propertyFile;
        if(externalDefines == null)
            EXTERNAL_DEFINES = new LinkedList<>();
        else
            EXTERNAL_DEFINES = externalDefines;
        FAIL_ON_WARNING = failOnWarning;
    }
    public abstract FormalOutput run() throws CompilerException;
}
