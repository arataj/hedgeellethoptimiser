/*
 * Prism.java
 *
 * Created on Jun 9, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser.formal;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PrismExec.Result;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;
import pl.gliwice.iitis.hedgeelleth.tools.PAdversary;

/**
 * Runs the Prism tool, interpretes the results.
 * 
 * @author Artur Rataj
 */
public class Prism extends AbstractFormal {
    /**
     * A type of the input model.
     */
    public final ModelType MODEL_TYPE;
    /**
     * Default Prism executable. Override with <code>setExecutable()</code>.
     */
    final public static String DEFAULT_EXECUTABLE = "prism";
    
    protected String executable;
    
    /**
     * Creates a new interface for Prism, with the default executable path
     * of <code>DEFAULT_EXECUTABLE</code>/
     * 
     * @param options options, does not include any of the input files
     * @param modelFiles the model input files
     * @param propertyFile the property input files
     * @param externalDefines external defines in the form as specified in
     * <code>InputModel.getExternalDefines()</code>; null or empty list for none
     * @param failOnWarning if to fail on a warning from Prism; see that there
     * is always a failure in case of an error from Prism
     */
    public Prism(String[] options, ModelType modelType,
            String[] modelFiles, String propertyFile,
            List<String> externalDefines, boolean failOnWarning) {
        super(options, modelFiles, propertyFile, externalDefines, failOnWarning);
        MODEL_TYPE = modelType;
        setExecutable(DEFAULT_EXECUTABLE);
    }
    /**
     * Sets the path of the executable.
     * 
     * @param newExecutable name path
     */
    public void setExecutable(String newExecutable) {
        executable = newExecutable;
    }
    @Override
    public FormalOutput run() throws CompilerException {
        FormalOutput out = new FormalOutput();
        ExecProcess exec = new PrismExec(FAIL_ON_WARNING);
        List<String> cliArgs = new LinkedList<>();
        cliArgs.add(executable);
        cliArgs.addAll(Arrays.asList(OPTIONS));
        cliArgs.addAll(Arrays.asList(MODEL_FILES));
        cliArgs.add(PROPERTY_FILE);
        if(!EXTERNAL_DEFINES.isEmpty()) {
            cliArgs.add("-const");
            boolean first = true;
            StringBuilder defines = new StringBuilder();
            for(String d : EXTERNAL_DEFINES) {
                if(first)
                    first = false;
                else
                    defines.append(",");
                defines.append(d);
            }
            cliArgs.add(defines.toString());
        }
        if(MODEL_FILES.length != 1)
            throw new CompilerException(null, CompilerException.Code.IO,
                    "Prism expects a single model file");
        out.modelFilename = MODEL_FILES[0];
        String outPrefix = CompilerUtils.getFileNameFromPath(out.modelFilename);
        final String EXTENSION_ADV = "adv";
        final String EXTENSION_STRATEGY = "strategy";
        try {
            out.stateFilename = StringAnalyze.replaceExtension(outPrefix, "sta");
            out.labelsFilename = StringAnalyze.replaceExtension(out.stateFilename, "label");
            out.xFilename = StringAnalyze.replaceExtension(outPrefix, "tra");
            switch(MODEL_TYPE) {
                case MDP:
                    out.advFilename = StringAnalyze.replaceExtension(outPrefix,
                            EXTENSION_ADV);
                    break;
                    
                case SMG:
                    out.strategyFilename = StringAnalyze.replaceExtension(outPrefix,
                            EXTENSION_STRATEGY);
                    break;
                    
                default:
                    throw new CompilerException(null, CompilerException.Code.IO,
                            "model type not implemented by Prism backend: " +
                            MODEL_TYPE.getHeader());
            }
        } catch(IOException e) {
            throw new CompilerException(null, CompilerException.Code.IO,
                    "malformed model filename: " + e.getMessage());
        }
        cliArgs.add("-exportstates");
        cliArgs.add(out.stateFilename);
        cliArgs.add("-exportlabels");
        cliArgs.add(out.labelsFilename);
        cliArgs.add("-exporttrans");
        cliArgs.add(out.xFilename);
        cliArgs.add("-exportadvmdp");
        if(out.strategyFilename != null)
            cliArgs.add(out.strategyFilename);
        else
            cliArgs.add(out.advFilename);
        exec.run(cliArgs);
        if(exec instanceof PrismExec) {
            PrismExec prismExec = (PrismExec)exec;
            Result result = prismExec.getResult();
            if(result.warning != null)
                System.out.println("while running Prism: WARNING: " + result.warning);
        }
        if(out.strategyFilename != null && out.advFilename == null) {
            // output files produced by prism-games
            try {
                out.advFilename = StringAnalyze.replaceExtension(outPrefix,
                        EXTENSION_ADV);
            } catch(IOException e) {
                throw new RuntimeException("unexpected: " + e.toString());
            }
            String[] args = {
                out.stateFilename,
                out.xFilename,
                out.strategyFilename,
                out.advFilename,
            };
            try {
                PAdversary.main(args);
            } catch(IOException e) {
                throw new CompilerException(null, CompilerException.Code.IO,
                        "could not convert strategy to adversary: " + e.getMessage());
            }
        }
        return out;
    }
}
