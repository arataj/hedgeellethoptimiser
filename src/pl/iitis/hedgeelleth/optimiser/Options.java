/*
 * Options.java
 *
 * Created on Jun 6, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

import java.util.*;

import java.util.*;

import com.martiansoftware.jsap.*;
import pl.gliwice.iitis.hedgeelleth.About;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ModelType;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.math.MultivariatePolynomial;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;
import pl.iitis.hedgeelleth.optimiser.formal.AbstractFormal;
import pl.iitis.hedgeelleth.optimiser.formal.Prism;
import pl.iitis.hedgeelleth.optimiser.mf.AbstractMF;

/**
 * Command--line options.
 * 
 * @author Artur Rataj
 */
public class Options {
    /**
     * An option to prepend Hc options, removed before an option is
     * passed to Hc.
     */
    final static String HC_OPTION_PREFIX = "-C";
    /**
     * An option to prepend model checker options, removed before an option is
     * passed to the model checker.
     */
    final static String MC_OPTION_PREFIX = "-M";
    /**
     * The option -f&lt;FORMAL_UTILITY_PRISM&gt; selects Prism.
     */
    final static char FORMAL_UTILITY_PRISM = 'p';
    /**
     * The option -a&lt;APPROX_OPTIMISATION_SIMPLEX_NELDER_MEAD&gt; selects
     * the Nelder-Mead simplex method.
     */
    final static char APPROX_OPTIMISATION_SIMPLEX_NELDER_MEAD = 'n';
    /**
     * The option -a&lt;APPROX_OPTIMISATION_SIMPLEX_MULTIDIRECTIONAL&gt; selects
     * the multidirectional simplex method.
     */
    final static char APPROX_OPTIMISATION_SIMPLEX_MULTIDIRECTIONAL = 'm';
    /**
     * The option -a&lt;APPROX_OPTIMISATION_BOBYQA&gt; selects
     * the BOBYQA method.
     */
    final static char APPROX_OPTIMISATION_BOBYQA = 'b';
    /**
     * The option -a&lt;APPROX_OPTIMISATION_CMAES&gt; selects
     * the CMAES method.
     */
    final static char APPROX_OPTIMISATION_CMAES = 'c';
    /**
     * The option -a&lt;APPROX_OPTIMISATION_BFGS&gt; selects
     * the CMAES method.
     */
    final static char APPROX_OPTIMISATION_BFGS = 'f';
    /**
     * Format of the 2nd stage decision boundary, printed at output: plain.
     */
    final static char BOUNDARY_EQUATION_STYLE_PLAIN = 'p';
    /**
     * Format of the 2nd stage decision boundary, printed at output: Java math functions.
     */
    final static char BOUNDARY_EQUATION_STYLE_JAVA = 'j';
    /**
     * Format of the 2nd stage decision boundary, printed at output: coefficients in order
     * compatible with <code>MathUtil.findMultivariatePolyPowers()</code>.
     */
    final static char BOUNDARY_EQUATION_STYLE_COEFFICIENTS = 'c';

    /**
     * Checker constants.
     */
    public enum Checker {
        PRISM,
    };
    /**
     * Approximate optimisation constants.
     */
    public enum Approx {
        SIMPLEX_NELDER_MEAD,
        SIMPLEX_MULTIDIRECTIONAL,
        BOBYQA,
        CMAES,
        BFGS,
    };
    /**
     * Class of the decision boundary in the 2nd stage.
     */
    public enum ApproxDecisionBoundary {
        HYPERPLANE,
        // order specified separately
        POLYNOMIAL,
    };
    /**
     * Computation method constants.
     */
    public enum Method {
        FORMAL_EXTRAPOLATE,
    }
    /**
     * Java source file extension. Defined here to avoid dependencies on Hc.
     */
    public final static String JAVA_FILE_EXTENSION = "java";
    /**
     * Hc Nd file extension. Defined here to avoid dependencies on Hc.
     */
    public final static String HC_ND_FILE_EXTENSION = "nd";
    /**
     * Prism model file extension. Defined here to avoid dependencies on Hc.
     */
    public final static String PRISM_MODEL_FILE_EXTENSION = "nm";
    /**
     * Prism property file extension. Defined here to avoid dependencies on Hc.
     */
    public final static String PRISM_PROPERTY_FILE_EXTENSION = "pctl";
    
    /**
     * If only a help message shoul be displayed. The default if <code>false</code>.
     */
    public boolean help = false;
    /**
     * Constants for formal verification.
     */
    public CliConstants formalDefines;
    /**
     * Path of Prism executable.
     */
    public String prismExecutablePath;
    /**
     * Constants for approximate verification.
     */
    public CliConstants approxDefines;
    /**
     * Copied to <code>AbstractFormal.FAIL_ON_WARNING</code>.
     */
    public boolean failOnWarning;
    /**
     * A model checker, 1st stage optimisation.
     */
    public Checker checker;
    /**
     * An approximate optimisation, 2nd stage optimisation.
     */
    public Approx approx;
    /**
     * If 0, no multistart optimiser is used. Otherwise it is used with
     * a given number of restarts.
     */
    public int approxRestarts;
    /**
     * Class of the decision boundary in the 2nd stage.
     */
    public ApproxDecisionBoundary approxDecisionBoundary;
    /**
     * If <code>approxDecisionBoundary == POLYNOMIAL</code> then its order,
     * otherwise 0.
     */
    public int approxDecisionBoundaryPolynomialOrder;
    /**
     * Method to use.
     */
    public Method method;
    /**
     * If true, print the boundary equation using Java math functions. One of the constants
     * <code>BOUNDARY_EQUATION_STYLE_*</code>.
     */
    public MultivariatePolynomial.OutFormat boundaryEquationStyle;
    /**
     * If false, all temporary files are removed.
     */
    public boolean keepTemporaryFiles;
    /**
     * Input files.
     */
    public List<String> files;
    /**
     * Hc--specific options filtered from CLI arguments.
     */
    public String[] hcArgs;
    /**
     * Model checker--specific options filtered from CLI arguments.
     */
    public String[] mcArgs;
    /**
     * This application--specific options filtered from CLI arguments.
     */
    public static String[] optimiserArgs;
    
    /**
     * Parses CLI options.
     * 
     * @param cliOptions command--line options
     */
    public Options(String[] cliOptions) throws CompilerException {
        splitArgs(cliOptions);
        parseOptions();
    }
    /**
     * Splits command--line arguments between optimiser-- and hc--specific
     * ones.
     * 
     * @param args arguments of the <code>main()</code> function
     */
    private void splitArgs(String[] args) {
        int oLength = 0;
        int hcLength = 0;
        int mcLength = 0;
        for(String a : args)
            if(a.startsWith(HC_OPTION_PREFIX))
                ++hcLength;
            else if(a.startsWith(MC_OPTION_PREFIX))
                ++mcLength;
            else
                ++oLength;
        optimiserArgs = new String[oLength];
        hcArgs = new String[hcLength];
        mcArgs = new String[mcLength];
        int oIndex = 0;
        int hcIndex = 0;
        int mcIndex = 0;
        for(String a : args)
            if(a.startsWith(HC_OPTION_PREFIX))
                hcArgs[hcIndex++] = a.substring(HC_OPTION_PREFIX.length());
            else if(a.startsWith(MC_OPTION_PREFIX))
                mcArgs[mcIndex++] = a.substring(MC_OPTION_PREFIX.length());
            else
                optimiserArgs[oIndex++] = a;
    }
    /**
     * Parses <code>optimiserArgs</code>. If a help option is given, full syntax of arguments
     * is displayed to the stdout.
     * 
     * @return optimiser options
     */
    private void parseOptions() throws CompilerException {
            final String HELP = "help";
            final String GET_FORMAL_CONST = "formal verification constants";
            final String GET_APPROX_CONST = "approximate optimisation constants";
            final String CONTINUE_WARNING = "continuel on warning";
            final String METHOD = "method to use";
            final String UTILITY_FORMAL = "1st stage: formal model checker to use";
            final String PRISM_EXECUTABLE = "prism executable, default " + Prism.DEFAULT_EXECUTABLE;
            final String APPROX_OPTIMISATION = "2nd stage: approximate optimisation method";
            final String DECISION_BOUNDARY_CLASS = "2nd stage: class of decision boundary";
            final String KEEP_TEMPORARY = "do not remove temporary files";
            final String BOUNDARY_EQUATION_STYLE = "print formula of decision boundary using:\n\tp\tplain\n\tj\tjava math functions\n\tc\tonly coefficients, order as in findMultivariatePolyPowers()";
            final String INPUT_FILES = "source_file_";
            try {
                JSAP jsap = new JSAP();
                //
                // cli options
                //
                // -h -- help
                Switch helpParam = new Switch(HELP).
                    setShortFlag('h').
                    setLongFlag("help");
                helpParam.setHelp("show help and exit");
                jsap.registerParameter(helpParam);
                // -F, --formal-define
                final String CONST_HELP =  "<name>=<value(s)>,... " +
                        "where <value(s)> is an alphanumeric constant or a set of " +
                        "integer values <min>~<max>[:<step>]";
                FlaggedOption constParamFormal = new FlaggedOption(GET_FORMAL_CONST)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setRequired(false) 
                                        .setShortFlag('F') 
                                        .setLongFlag("formal-define")
                                        .setList(true)
                                        .setUsageName("define")
                                        .setListSeparator(',');
                constParamFormal.setHelp("constant definitions for 1st stage, formal verification: " +
                        CONST_HELP);
                constParamFormal.setCategory("MODEL");
                jsap.registerParameter(constParamFormal);
                // -A, --approximate-define
                FlaggedOption constParamApprox = new FlaggedOption(GET_APPROX_CONST)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setRequired(false) 
                                        .setShortFlag('A') 
                                        .setLongFlag("approximate-define")
                                        .setList(true)
                                        .setUsageName("define")
                                        .setListSeparator(',');
                constParamApprox.setHelp("constant definitions for 2nd stage, approximate optimisation: " +
                        CONST_HELP);
                constParamApprox.setCategory("MODEL");
                jsap.registerParameter(constParamApprox);
                // -w --continue-on-warning
                Switch continueOnWarning = new Switch(CONTINUE_WARNING).
                    setShortFlag('w').
                    setLongFlag("continue-on-warning");
                continueOnWarning.setHelp("continue despite a major warning from the checker; still fail on error");
                continueOnWarning.setCategory("MODEL");
                jsap.registerParameter(continueOnWarning);
                // -m --method
                final String[] methodNames = {
                    "fe",
                };
                final String[] methodDescription = {
                    "1st formal 2nd extrapolate",
                };
                FlaggedOption methodParam = new FlaggedOption(METHOD)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setDefault(methodNames[0]) 
                                        .setRequired(false)
                                        .setShortFlag('m')
                                        .setLongFlag("method")
                                        .setList(false);
                StringBuilder h = new StringBuilder();
                Iterator<String> mdI = Arrays.asList(methodDescription).iterator();
                for(String s : methodNames) {
                    h.append("\n\t");
                    h.append(s);
                    h.append(" ");
                    h.append(mdI.next());
                }
                methodParam.setHelp(METHOD + ":" + h);
                methodParam.setCategory("OPTIMISATION");
                jsap.registerParameter(methodParam);
                // -f --formal
                FlaggedOption formalUtility = new FlaggedOption(UTILITY_FORMAL)
                                        .setStringParser(JSAP.CHARACTER_PARSER)
                                        .setDefault("" + FORMAL_UTILITY_PRISM) 
                                        .setRequired(false) 
                                        .setShortFlag('f') 
                                        .setLongFlag("formal");
                formalUtility.setHelp(
                        FORMAL_UTILITY_PRISM + " Prism");
                formalUtility.setCategory("OPTIMISATION");
                jsap.registerParameter(formalUtility);
                // --prism-executable
                FlaggedOption prismExecutable = new FlaggedOption(PRISM_EXECUTABLE)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setDefault(Prism.DEFAULT_EXECUTABLE) 
                                        .setRequired(false) 
                                        .setLongFlag("prism-executable");
                prismExecutable.setHelp(PRISM_EXECUTABLE);
                prismExecutable.setCategory("OPTIMISATION");
                jsap.registerParameter(prismExecutable);
                // -a --approximate
                FlaggedOption approximateMethod = new FlaggedOption(APPROX_OPTIMISATION)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setDefault("" + APPROX_OPTIMISATION_SIMPLEX_NELDER_MEAD) 
                                        .setRequired(false) 
                                        .setShortFlag('a') 
                                        .setLongFlag("approximate");
                approximateMethod.setHelp(APPROX_OPTIMISATION + ":<type>[<num restarts>]\n" +
                        "\t" + APPROX_OPTIMISATION_SIMPLEX_NELDER_MEAD + "\tNelder-Mead simplex\n" +
                        "\t" + APPROX_OPTIMISATION_SIMPLEX_MULTIDIRECTIONAL + "\tmultidirectional simplex\n" +
                        "\t" + APPROX_OPTIMISATION_BOBYQA + "\tBOBYQA\n" +
                        "\t" + APPROX_OPTIMISATION_CMAES + "\tCMAES" +
                        "\t" + APPROX_OPTIMISATION_BFGS + "\tBFGS (unimplemented)");
                approximateMethod.setCategory("OPTIMISATION");
                jsap.registerParameter(approximateMethod);
                // -d --decision-boundary
                FlaggedOption decisionBoundary = new FlaggedOption(DECISION_BOUNDARY_CLASS)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setDefault("1") 
                                        .setRequired(false) 
                                        .setShortFlag('d') 
                                        .setLongFlag("decision-boundary");
                decisionBoundary.setHelp(
                        "1 hyperplane, >1 multivariate polynomial");
                decisionBoundary.setCategory("OPTIMISATION");
                jsap.registerParameter(decisionBoundary);
                // -B --out-boundary-format
                FlaggedOption boundaryStyleParam = new FlaggedOption(BOUNDARY_EQUATION_STYLE)
                                        .setStringParser(JSAP.CHARACTER_PARSER)
                                        .setDefault("" + BOUNDARY_EQUATION_STYLE_PLAIN) 
                                        .setRequired(false) 
                                        .setShortFlag('B') 
                                        .setLongFlag("out-boundary-format");
                boundaryStyleParam.setHelp(BOUNDARY_EQUATION_STYLE);
                boundaryStyleParam.setCategory("OUTPUT");
                jsap.registerParameter(boundaryStyleParam);
                // -K --keep-temporary
                Switch keepTemporaryParam = new Switch(KEEP_TEMPORARY).
                    setShortFlag('K').
                    setLongFlag("keep-temporary");
                keepTemporaryParam.setHelp(KEEP_TEMPORARY);
                keepTemporaryParam.setCategory("OUTPUT");
                jsap.registerParameter(keepTemporaryParam);
                // input files
                UnflaggedOption inputFilesParam = new UnflaggedOption(INPUT_FILES)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setRequired(false)
                                        .setGreedy(true);
                inputFilesParam.setCategory("");
                jsap.registerParameter(inputFilesParam);
                inputFilesParam.setHelp("source files: " +
                        "*." + JAVA_FILE_EXTENSION);
                //
                JSAPResult config = jsap.parse(optimiserArgs);
                if(!config.success()) {
                    Iterator<String> it = (Iterator<String>)config.getErrorMessageIterator();
                    StringBuilder errors = new StringBuilder();
                    while(it.hasNext()) {
                        if(errors.length() != 0)
                            errors.append("; ");
                        errors.append(it.next());
                    }
                    throw new JSAPException(errors.toString() + "; use -h for help");
                }
                help = config.getBoolean(HELP);
                formalDefines = pl.gliwice.iitis.hedgeelleth.cli.Main.parseCLIConstants(
                        config.getStringArray(GET_FORMAL_CONST), true);
                prismExecutablePath = config.getString(PRISM_EXECUTABLE);
                approxDefines = pl.gliwice.iitis.hedgeelleth.cli.Main.parseCLIConstants(
                        config.getStringArray(GET_APPROX_CONST), true);
                failOnWarning = !config.getBoolean(CONTINUE_WARNING);
                String methodName = config.getString(METHOD);
                int index = 0;
                for(String s : methodNames) {
                    if(methodName.equals(s)) {
                        switch(index) {
                            case 0:
                                method = Method.FORMAL_EXTRAPOLATE;
                                break;
                                
                            default:
                                throw new JSAPException("unknown method " + methodName);
                        }
                    }
                    ++index;
                }
                switch(config.getChar(UTILITY_FORMAL)) {
                    case FORMAL_UTILITY_PRISM:
                        checker = Options.Checker.PRISM;
                        break;
                        
                    default:
                        throw new JSAPException("unknown model checker " +
                                config.getChar(UTILITY_FORMAL));
                }
                String approxStr = config.getString(APPROX_OPTIMISATION);
                if(approxStr.isEmpty())
                    throw new JSAPException("missing optimizer type");
                char approxType = approxStr.charAt(0);
                if(approxStr.length() > 1) {
                    String numStr = approxStr.substring(1);
                    try {
                        approxRestarts = Integer.parseInt(numStr);
                    } catch(NumberFormatException e) {
                        throw new JSAPException("invalid number of restarts of the optimizer");
                    }
                } else
                    approxRestarts = 0;
                switch(approxType) {
                    case APPROX_OPTIMISATION_SIMPLEX_NELDER_MEAD:
                        approx = Options.Approx.SIMPLEX_NELDER_MEAD;
                        break;
                        
                    case APPROX_OPTIMISATION_SIMPLEX_MULTIDIRECTIONAL:
                        approx = Options.Approx.SIMPLEX_MULTIDIRECTIONAL;
                        break;
                        
                    case APPROX_OPTIMISATION_BOBYQA:
                        approx = Options.Approx.BOBYQA;
                        break;
                        
                    case APPROX_OPTIMISATION_CMAES:
                        approx = Options.Approx.CMAES;
                        break;
                        
                    case APPROX_OPTIMISATION_BFGS:
                        approx = Options.Approx.BFGS;
                        break;
                        
                    default:
                        throw new JSAPException("unknown approximate optimisation " +
                                config.getChar(UTILITY_FORMAL));
                }
                String dbc = config.getString(DECISION_BOUNDARY_CLASS);
                int dbcOrder;
                try {
                    dbcOrder = Integer.parseInt(dbc);
                } catch(NumberFormatException e) {
                    dbcOrder = 0;
                }
                if(dbcOrder == 0) {
                    switch(dbc) {
                        default:
                            throw new JSAPException("unknown class of decision boundary: " +
                                    "`" + dbc + "'");
                    }
                } else if(dbcOrder == 1) {
                    approxDecisionBoundary = ApproxDecisionBoundary.HYPERPLANE;
                    approxDecisionBoundaryPolynomialOrder = 0;
                } else {
                    approxDecisionBoundary = ApproxDecisionBoundary.POLYNOMIAL;
                    approxDecisionBoundaryPolynomialOrder = dbcOrder;
                }
                switch(config.getChar(BOUNDARY_EQUATION_STYLE)) {
                    case BOUNDARY_EQUATION_STYLE_PLAIN:
                        boundaryEquationStyle = MultivariatePolynomial.OutFormat.PLAIN;
                        break;
                        
                    case BOUNDARY_EQUATION_STYLE_JAVA:
                        boundaryEquationStyle = MultivariatePolynomial.OutFormat.JAVA;
                        break;
                        
                    case BOUNDARY_EQUATION_STYLE_COEFFICIENTS:
                        boundaryEquationStyle = MultivariatePolynomial.OutFormat.FIND_MULTIVARIATE_POLY_POWERS;
                        break;
                        
                    default:
                        throw new RuntimeException("unknown boundary output format");
                }
                keepTemporaryFiles = config.getBoolean(KEEP_TEMPORARY);
                files = CompilerUtils.expandFiles(
                        Arrays.asList(config.getStringArray(INPUT_FILES)));
                if(help) {
                    System.out.println("Verics two stage optimiser " +
                            About.VERSION + "\n\n" +
                            "command line syntax:\n\n" +
                            "  ( " + HC_OPTION_PREFIX + "<*> )?\n\t<*>'s passed to veric, e.g. -C-sh -C-i\n" +
                            "  ( " + MC_OPTION_PREFIX + "<*> )?\n\t<*>'s passed to model checker\n" +
                            jsap.getHelp());
                }
            } catch(JSAPException e) {
                throw new CompilerException(null, CompilerException.Code.INVALID,
                        e.getMessage());
            }
    }
   /**
    * Returns the formal interface, as specified by <code>checker</code>.
    * 
    * @param input model description of input model
    * @param executable if not null, changes the default executable
    * of <code>Prism.DEFAULT_EXECUTABLE</code>
    * 
    * @return an interface for interacting with a checker
    */
    public AbstractFormal getFormal(InputModel inputModel, String executable) {
        String[] modelFileArray = { inputModel.getModelFile(), };
        ModelType modelType = inputModel.getType();
        String[] modelFiles = modelFileArray;
        String propertyFile = inputModel.getPropertyFile();
        List<String> externalDefines = inputModel.getExternalDefines();
        //hcOut.modelFiles.toArray(new String[hcOut.modelFiles.size()]),
        switch(checker) {
            case PRISM:
                Prism prism = new Prism(mcArgs, modelType, modelFiles, propertyFile,
                        externalDefines, failOnWarning);
                if(executable != null)
                    prism.setExecutable(executable);
                return prism;
                
            default:
                throw new RuntimeException("unknown checker");
        }
    }
}
