/*
 * NdChoice.java
 *
 * Created on Jun 17, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile.Branch;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile.Choice;
import pl.gliwice.iitis.hedgeelleth.tools.PAdversary;
import pl.gliwice.iitis.hedgeelleth.tools.PAdversary.Transition;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * Nondeterministic choice extracted from an adversary file using
 * a Nd file.
 * 
 * @author Artur Rataj
 */
public class NdChoice {
    /**
     * List of choice states.
     */
    final public List<Integer> STATE;
    /**
     * Branch at a subsequent state, being a return value of
     * <code>Random.nextInt()</code>.
     */
    final public List<Branch> BRANCH;
    /**
     * Creates a new empty choice.
     */
    public NdChoice() {
        STATE = new ArrayList<>();
        BRANCH = new ArrayList<>();
    }
    /**
     * Creates a new choice from nd file and adversary.
     * 
     * @param nd nd file
     * @param adv adversary
     */
    public NdChoice(NdFile nd, PAdversary adv) throws CompilerException {
        this();
        for(long s : adv.transitionsMap.keySet()) {
            if(s > Integer.MAX_VALUE)
                throw new CompilerException(null, CompilerException.Code.ILLEGAL,
                        "index state too large");
            int sourceIndex = (int)s;
            int[] source = adv.states.getState(sourceIndex);
            List<Transition> xs = adv.transitionsMap.get(s);
            for(Transition x : xs) {
                if(x.targetIndex > Integer.MAX_VALUE)
                    throw new CompilerException(null, CompilerException.Code.ILLEGAL,
                            "index state too large");
                int targetIndex = (int)x.targetIndex;
                int[] target = adv.states.getState(targetIndex);
                for(Choice ch : nd.CHOICES) {
                    boolean first = true;
                    int stepIndex = -1;
                    for(Branch b : ch.xs) {
                        if(first) {
                            stepIndex = adv.states.names.indexOf(b.stepVariable);
                        }
                        if(b.source == source[stepIndex] && b.target == target[stepIndex]) {
                            STATE.add(sourceIndex);
                            BRANCH.add(b);
                        }
                        first = false;
                    }
                }
            }
        }
    }
}
