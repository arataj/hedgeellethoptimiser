/*
 * Hc.java
 *
 * Created on Jun 9, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

import java.util.*;
import java.io.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.ExecProcess;
import pl.gliwice.iitis.hedgeelleth.compiler.util.HcOutputFiles;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * An interface layer that runs <code>hc</code>.
 *
 * @author Artur Rataj
 */
public class Hc extends ExecProcess {
    /**
     * A command to run <code>hc</code>.
     */
    protected static String HC_COMMAND = "hc";
    /**
     * All cli arguments, including the system command.
     */
    protected static List<String> cliArgs;
    /**
     * Hc's output files.
     */
    HcOutputFiles hcOutFiles;
    
    /**
     * Prepares cli arguments.
     * 
     * @param options general options
     * @return the name of the output file
     */
    public Hc(Options options) {
        super(true);
        String[] cliOptions = options.hcArgs;
        String[] modelFiles = options.files.toArray(new String[1]);
        // hc's all cli arguments
        cliArgs = new LinkedList<>();
        cliArgs.addAll(ExecProcess.getShellPrefix());
        cliArgs.add(HC_COMMAND);
        cliArgs.addAll(Arrays.asList(cliOptions));
        String checkerOption;
        switch(options.checker) {
            case PRISM:
                checkerOption = "p";
                break;
                
            default:
                throw new RuntimeException("unknown checker");
        }
        // output to a given checker
        cliArgs.add("-o" + checkerOption);
        // generate a state class
        cliArgs.add("-j");
        // generate nd file
        cliArgs.add("-n");
        // display all output files
        cliArgs.add("-R");
        cliArgs.addAll(Arrays.asList(modelFiles));
    }
    @Override
    protected void processOut(BufferedReader br) throws CompilerException {
        hcOutFiles = new HcOutputFiles(br);
    }
    /**
     * Runs <code>hc</code>, returns the names of the output files.
     * 
     * @param namedDefines defines of the form &lt;name&gt;=&ltvalue&gt;, empty list
     * for none
     * @return file name
     */
    public HcOutputFiles runHc(List<String> namedDefines) throws CompilerException {
        List<String> ca = new LinkedList<>(cliArgs);
        if(!namedDefines.isEmpty()) {
            ca.add("--const");
            StringBuilder s = new StringBuilder();
            boolean first = true;
            for(String d : namedDefines) {
                if(!first)
                    s.append(",");
                s.append(d);
                first = false;
            }
            ca.add(s.toString());
        }
        run(ca);
        return hcOutFiles;
    }
}
