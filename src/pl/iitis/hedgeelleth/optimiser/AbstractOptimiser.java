/*
 * AbstractOptimiser.java
 *
 * Created on Jun 6, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * An abstract optimiser.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractOptimiser {
    /**
     * Command--line options.
     */
    final protected Options OPTIONS;
    /**
     * A formal model with resolved non--determinism, to be optimised.
     */
    final protected OptimisedModel MODEL;
    
    public AbstractOptimiser(Options options, OptimisedModel model) {
        OPTIONS = options;
        MODEL = model;
    }
    /**
     * An optimisation method.
     */
    protected abstract void process() throws CompilerException;
//    /**
//     * Returns all files generated within <code>process()</code>,
//     * in a single list.
//     * 
//     * @return list of file names
//     */
//    public List<String> getGeneratedFiles() {
//        return GENERATED_FILES;
//    }
}
