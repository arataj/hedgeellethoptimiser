/*
 * OptimisedModel.java
 *
 * Created on Jun 20, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.hedgeelleth.optimiser;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.NdFile;
import pl.gliwice.iitis.hedgeelleth.tools.PStates;

/**
 * Model's data, with resolved non--determinism. Required to optimise the equations
 * which would replace nd functions.
 * 
 * @author Artur Rataj
 */
public class OptimisedModel {
    /**
     * Description of non--determinism in the model.
     */
    public final NdFile ND_FILE;
    /**
     * States of the model.
     */
    public final PStates STATES;
    /**
     * Choice to optimise a function to.
     */
    public final NdChoice CHOICE;
    /**
     * Indices of subsequent nd variables in subsequent choices. The size of this map is equal to the size
     * of the list <code>ND_FILE.CHOICES</code>. An array contains placeholders for parameters so that
     * array key is argument index. For a parameter the placeholder index is -1.
     */
    public final Map<NdFile.Choice, int[]> VAR_INDEX;
    
    
    /**
     * Creates an object with model's data, required to optimise the model
     * by replacing non-determinism with a function.
     * 
     * @param ndFile description of non--determinism in the model
     * @param states states of the model
     * @param choice choice to optimise a function to
     */
    public OptimisedModel(NdFile ndFile, PStates states, NdChoice choice) {
        ND_FILE = ndFile;
        STATES = states;
        CHOICE = choice;
        VAR_INDEX = new HashMap<>();
        for(NdFile.Choice ch : ND_FILE.CHOICES)
            VAR_INDEX.put(ch, getArgIndices(ch));
    }
    /**
     * Returns nd var indices for a gicen choice.
     * 
     * @param ch choice
     * @return array of indices, its length equal to that of <code>ch.argName</code>;
     * for a respective parameter the index is -1
     * 
     */
    protected final int[] getArgIndices(NdFile.Choice ch) {
        Iterator<Number> valueI = ch.argValue.iterator();
        int[] indices = new int[ch.argName.size()];
        int count = 0;
        for(String name : ch.argName) {
            Number num = valueI.next();
            int index;
            if(num != null)
                index = -1;
            else {
                index = STATES.names.indexOf(name);
                if(index == -1)
                    throw new RuntimeException("state variable not found: " + name);
            }
            indices[count++] = index;
        }
        return indices;
    }
    /**
     * Check if all choices have binary branches with values 0 and 1. Required by some
     * solvers.
     * 
     * @return null if true, otherwise textual description of disrepancy
     */
    public String check01Choices() {
        for(NdFile.Choice ch : ND_FILE.CHOICES) {
            if(ch.xs.size() != 2)
                return "expected binary choices, found " + ch.toString();
            for(NdFile.Branch b : ch.xs) {
                if(b.num.size() != 1)
                    return "expected single--valued branches, found " + ch.toString();
                int i = b.num.get(0);
                if(i != 0 && i != 1)
                    return "expected branch values 0 or 1, found " + ch.toString();
            }
        }
        return null;
    }
}
