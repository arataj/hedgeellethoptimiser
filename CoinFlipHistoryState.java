package example.sim;

import java.lang.reflect.Field;

public class CoinFlipHistoryState {
    private static Field getField(Class clazz, String fieldName)
            throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
    hc.AbstractBeam o1;
    Field f2;
    hc.AbstractBeam o4;
    Field f5;
    hc.AbstractBeam o7;
    Field f8;
    public CoinFlipHistoryState() {
        try {
            Field f0 = getField(hc.AbstractBeam.class, "staticHook");
            f0.setAccessible(true);
            hc.AbstractBeam[] o0 = (hc.AbstractBeam[])f0.get(null);
            o1 = o0[0];
            f2 = getField(example.sim.CoinFlipHistory.class, "flips");
            f2.setAccessible(true);
            Field f3 = getField(hc.AbstractBeam.class, "staticHook");
            f3.setAccessible(true);
            hc.AbstractBeam[] o3 = (hc.AbstractBeam[])f3.get(null);
            o4 = o3[0];
            f5 = getField(example.sim.CoinFlipHistory.class, "heads");
            f5.setAccessible(true);
            Field f6 = getField(hc.AbstractBeam.class, "staticHook");
            f6.setAccessible(true);
            hc.AbstractBeam[] o6 = (hc.AbstractBeam[])f6.get(null);
            o7 = o6[0];
            f8 = getField(example.sim.CoinFlipHistory.class, "latestHead");
            f8.setAccessible(true);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int flips() {
        try {
            return f2.getInt(o1);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int heads() {
        try {
            return f5.getInt(o4);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int latestHead() {
        try {
            return f8.getInt(o7);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    public double get(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            case 2:
                return latestHead();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int getInt(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            case 2:
                return latestHead();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public long getLong(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            case 2:
                return latestHead();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int size() {
        return 3;
    }
    public String getName(int index) {
        switch(index) {
            case 0:
                return "flips";
            case 1:
                return "heads";
            case 2:
                return "latestHead";
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
}
